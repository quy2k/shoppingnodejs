const express = require('express');
const router = express.Router()
const Category = require('../models/category')

/**
 * GET category index
 */
router.get('/', (req, res) => {
    // 
    Category.find((err, category) => {
        if (err) return console.log(err)
            // res.json(category)   
        res.render('admin/categories', { category: category })
    })
});

/**
 * GET add category
 */
router.get('/add-category', (req, res) => {
    const title = "";

    res.render('admin/add_category', {
        title: title
    })
})

/**
 * POST add category
 */
router.post('/add-category', (req, res) => {
    console.log("vao day");
    req.checkBody('title', 'Title must hhave a value !').notEmpty();

    console.log(req.body);
    var title = req.body.title;
    var slug = title.replace(/\s+/g, '-').toLowerCase();

    var errors = req.validationErrors();
    if (errors) {
        res.render('admin/add_category', {
            errors: errors,
            title: title
        })
    } else {
        Category.findOne({ slug: slug }, (err, category) => {
            if (category) {
                // res.flash('danger', 'Category title exits, Plz choose title another')
                res.send('hahah')
                res.redirect('/admin/categories');
            } else {
                const category = new Category({
                    title: title,
                    slug: slug
                });
                category.save(err => {
                    if (err)
                        return console.log(err);
                    Category.find((err, categories) => {
                        if (err) {
                            console.log(err)
                        } else {
                            req.app.locals.categories = categories
                        }
                        // console.log(app.locals.categories)
                    });
                    req.flash('success', 'Category Added!');
                    res.redirect('/admin/categories')
                })
            }
        });
    }
});

/**
 * 
 * GET edit page
 */
router.get('/edit-category/:slug', async(req, res) => {
    console.log(req.params.slug);
    await Category.findOne({ slug: req.params.slug }, (err, category) => {
        if (err) { return console.log(err) }
        console.log(category)
        res.render('admin/edit_category', { category: category })
    })

});

/**
 * 
 * POST edit category
 */
router.post('/edit-category/:slug', async(req, res) => {
    console.log(req.body)
    await Category.findByIdAndUpdate(req.body.id, {
        title: req.body.title,
        slug: req.body.title.replace(/\s+/g, '-').toLowerCase()
    }, (err, category) => {
        if (err) { return console.log(err) }
        Category.find((err, categories) => {
            if (err) {
                console.log(err)
            } else {
                req.app.locals.categories = categories
            }
            // console.log(app.locals.categories)
        });
        console.log(category)
        res.redirect('/admin/categories');
    })
});
/**
 * 
 * GET delete category
 */
router.get('/delete-category/:id', async(req, res) => {
    console.log(req.params.id)
    await Category.findByIdAndDelete(req.params.id,
        (err, category) => {
            if (err)
                return console.log(err)

            Category.find((err, categories) => {
                if (err) {
                    console.log(err)
                } else {
                    req.app.locals.categories = categories
                }
                // console.log(app.locals.categories)
            });

            res.redirect('/admin/categories');
        }
    )
});

////////////////////////////////
module.exports = router;