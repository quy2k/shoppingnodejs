const express = require('express');
const router = express.Router()
const path = require('path');
const fs = require('fs');
const mkdirp = require('mkdirp');
const resizeImg = require('resize-img');
//Get Product model
const Product = require('../models/product')
    //Get Category model
const Category = require('../models/category')

var rimraf = require("rimraf");


/**
 * GET product index
 */
router.get('/', async(req, res) => {
    var count;
    await Product.countDocuments((err, c) => {
        count = c;
        // console.log(c)
    });
    await Product.find((err, products) => {
        res.render('admin/products', {
            products: products,
            count: count

        });
    });

});


/**
 * GET add product
 */
router.get('/add-product', async(req, res) => {
    const title = "";
    const desc = "";
    const price = "";
    // const categories = "";
    await Category.find({}, (err, category) => {
        const categories = category
        res.render('admin/add_product', {
            title: title,
            desc: desc,
            price: price,
            categories: categories
        })
    })

});
/**
 * POST add product
 */
router.post('/add-product', async(req, res) => {

    try {
        const title = req.body.title;
        const slug = title.replace(/\s/g, '-').toLowerCase();
        const desc = req.body.desc;
        const price = parseFloat(req.body.price).toFixed(2);
        const category = req.body.category;
        // Get name image
        // console.log(req.files);
        var imageFile = req.files;
        var file = imageFile.fileName;
        var nameImage = req.files !== null ? file.name : "";
        // console.log(nameImage)
        // console.log(imageFile)

        const newProduct = new Product({
            title: title,
            slug: slug,
            desc: desc,
            price: price,
            category: category,
            image: nameImage
        })
        newProduct.save((err, prooduct) => {
            if (err) console.log(err)

            console.log(prooduct.id);
            // var imageProductIdFile = prooduct.id;
            // Create new path for ProductId
            fs.mkdirSync(`public/product_images/${prooduct.id}`, { recursive: true })
            fs.mkdirSync(`public/product_images/${prooduct.id}/gallery`, { recursive: true })
            fs.mkdirSync(`public/product_images/${prooduct.id}/gallery/thumbs`, { recursive: true })
            file.mv(`./public/product_images/${prooduct.id}/` + nameImage, err => {
                if (err) console.log(err)
            })
            req.flash('success', 'OK!');
            res.redirect('/admin/products')



        })
    } catch (e) {
        console.log(e);
    }
})

/**
 * POST add category
 */
// router.post('/add-product', (req, res) => {
//     // if (req.files) {
//     //     console.log(req.files)
//     //     var file = req.files.file
//     //     var filename = file.name
//     //     console.log(filename)

//     // }
//     // else{
//     //     console.log(`No`)
//     // }
//     var imageFile = req.files !== undefined ? req.files.file.name : "";
//     console.log(imageFile);

//     req.checkBody('title', 'Title must have a value !').notEmpty();
//     req.checkBody('desc', 'Description must have a value !').notEmpty();
//     req.checkBody('price', 'Price must have a value !').isDecimal();

//     var title = req.body.title;
//     var slug = title.replace(/\s+/g, '-').toLowerCase();
//     var desc = req.body.desc;
//     var price = req.body.price;
//     var category = req.body.category;

//     var errors = req.validationErrors();
//     if (errors) {
//         Category.find((err, categories) => {
//             res.render('admin/add_product', {
//                 errors: errors,
//                 title: title,
//                 desc: desc,
//                 categories: categories,
//                 price: price
//             })
//         });
//     } else {
//         Product.findOne({ slug: slug }, (err, product) => {
//             if (product) {
//                 // res.flash('danger', 'Category title exits, Plz choose title another')
//                 Category.find((err, categories) => {
//                     res.render('admin/add_product', {
//                         errors: errors,
//                         title: title,
//                         desc: desc,
//                         categories: categories,
//                         price: price
//                     })
//                 });
//             }
//             else {

//                 var price2 = parseFloat(price).toFixed(2);

//                 const product = new Product({
//                     title: title,
//                     slug: slug,
//                     desc: desc,
//                     price: price2,
//                     category: category,
//                     image: imageFile
//                 });

//                 product.save(err => {
//                     if (err)
//                         return console.log(err);
//                     mkdirp('public/product_immage/' + product.id, err => {
//                         return console.log(err);
//                     });

//                     mkdirp('public/product_immage/' + product.id + '/gallery', err => {
//                         return console.log(err);
//                     });

//                     mkdirp('public/product_immage/' + product.id + '/gallery/thumbs', err => {
//                         return console.log(err);
//                     });

//                     if (imageFile != "") {
//                         var productImmage = req.files.image
//                         var path = 'public/product_image/' + product.id + '/' + imageFile;

//                         productImmage.mv(path, (err) => {
//                             return console.log(err);
//                         })
//                     }
//                     req.flash('success', 'Product Added!');
//                     res.redirect('/admin/products/add-product')
//                 })
//             }
//         });
//     }
// });

/**
 * 
 * GET edit product
 */
router.get('/edit-product/:id', async(req, res) => {
    // res.render('admin/edit_product')
    // console.log(req.params.id);
    // var errors="";
    // if (req.seesion.errors) errors = req.session.errors
    // req.session.errors = null;


    await Category.find({}, (err, category) => {
        Product.findById(req.params.id, (err, p) => {
            if (err) {
                console.log(err);
                res.redirect('/admin/products');
            } else {
                var galleryDir = 'public/product_images/' + p.id + '/gallery';
                var galleryImages = null;
                fs.readdir(galleryDir, (err, files) => {
                    if (err) {
                        console.log(err)
                    } else {
                        galleryImages = files;
                        const categories = category
                        res.render('admin/edit_product', {

                            id: p.id,
                            title: p.title,
                            desc: p.desc,
                            price: p.price,
                            category: p.category.replace(/\s+/g, '-').toLowerCase(),
                            image: p.image,
                            categories: categories,
                            galleryImages: galleryImages

                        })
                    }
                })
            }
        });
    })

});

/**
 * 
 * POST edit product
 */

router.post('/edit-product/:id', async(req, res) => {
    console.log(req.body);

    //Ckeck errors
    req.checkBody('title', 'Title must have a value !').notEmpty();
    req.checkBody('desc', 'Description must have a value !').notEmpty();
    req.checkBody('price', 'Price must have a value !').isDecimal();
    // req.checkBody('image', 'Image must upload an image !').isImage(nameImage);

    // console.log(req.body)
    const title = req.body.title;
    const slug = title.replace(/\s/g, '-').toLowerCase();
    const desc = req.body.desc;
    const price = parseFloat(req.body.price).toFixed(2);
    const category = req.body.category;
    const id = req.params.id;
    var errors = req.validationErrors();
    if (errors) {
        req.session.errors = errors;
        res.redirect('/admin/products/edit-product/' + id)
    } else {
        Product.findOne({ slug: slug, _id: id }, (err, p) => {
            if (err) {
                console.log(err)
            }
            if (p) {
                req.flash('danger', 'Product title already exists,choose another.');
                res.redirect('/admin/products/edit-product/' + id)
            } else {
                // Get name image
                // console.log(req.files);
                var imageFile = req.files;
                var nameImage = "";
                if (imageFile) {
                    console.log(`Đã chọn ảnh để thay đổi ảnh đại diện`)
                    var file = imageFile.image;
                    nameImage = file.name;

                    const dirPathRM = `public/product_images/${id}/`;

                    // console.log(dirPathRM);
                    // Xóa ảnh đại diện ban đầu
                    fs.readdir(dirPathRM, (err, files) => {
                            if (err)
                                console.log(err)
                            files.forEach(file => {
                                if (path.extname(file)) {
                                    // console.log(file)
                                    fs.unlinkSync(dirPathRM + file)
                                }
                            })
                        })
                        // Thêm ảnh đại diện mới
                    file.mv(`${dirPathRM}/${nameImage}`, (ere) => {
                        if (err) console.log(err);
                        console.log(`Thêm ảnh thành công!`);
                    })
                } else {
                    console.log(`Không thay đổi ảnh đại diện`)
                    nameImage = req.body.pimage;
                }
                console.log(nameImage);
                Product.findByIdAndUpdate(id, {
                        title: title,
                        slug: slug,
                        desc: desc,
                        price: price,
                        category: category,
                        image: nameImage
                    }, (err) => {
                        if (err)
                            console.log(err)
                        res.redirect('/admin/products');
                    }

                );
            };

        });
    }
});

/**
 * POST product gallery
 */
router.post('/product-gallery/:id', (req, res) => {
    var productImage = req.files.file;
    // console.log(productImage)
    var id = req.params.id;
    var path = 'public/product_images/' + id + '/gallery/' + productImage.name;
    var thumbsPath = 'public/product_images/' + id + '/gallery/thumbs/' + productImage.name;
    productImage.mv(path, (err) => {
        if (err) console.log(err)
        resizeImg(fs.readFileSync(path), { width: 150, height: 150 }).then(buf => {
            // console.log(buf)
            fs.writeFileSync(thumbsPath, buf);

        });
    });
    res.sendStatus(200);


});

/**
 * GET delete thumbs Image
 * 
 */

router.get('/delete-image/:img', (req, res) => {
    console.log(req.params.img)
    console.log(req.query.id)
    var path = 'public/product_images/' + req.query.id + '/gallery/' + req.params.img;
    var thumbsPath = 'public/product_images/' + req.query.id + '/gallery/thumbs/' + req.params.img;
    fs.rmSync(path)
    fs.rmSync(thumbsPath)
    res.redirect(`/admin/products/edit-product/${req.query.id}`)
});

/**
 * 
 * POST delete product
 */


router.get('/delete-product/:id', async(req, res) => {
    console.log(req.params.id)

    //remove FolderProductImageId
    const removeFolderProductImageId = `public/product_images/`;
    fs.readdir(removeFolderProductImageId, (err, files) => {
        if (err) console.log(err)
        files.forEach(file => {
            // console.log(file);
            if (file == req.params.id) {
                // console.log(file)
                rimraf(`${removeFolderProductImageId}/${file}`, (err) => {
                    if (err) console.log(err)
                });
                //Delete product Id
                Product.findByIdAndDelete(req.params.id,
                    (err, product) => {
                        if (err) { return console.log(err) }
                        console.log("redirect admin");
                        res.redirect('/admin/products');
                    });

            }
        });
    });

});

////////////////////////////////
module.exports = router;