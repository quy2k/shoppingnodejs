const express = require('express');
const router = express.Router()
    // Get Page model
var Page = require('../models/page');

/*
 * GET /
 */
router.get('/', async(req, res) => {
    await Page.findOne({ slug: "home" }, function(err, page) {
        if (err)
            console.log(err);
        // console.log(page)
        res.render('index', {
            title: page.title,
            content: page.content
        })

    });
});

/*
 * GET /
 */
router.get('/:slug', async(req, res) => {
    console.log(req.params.slug)
    await Page.findOne({ slug: req.params.slug }, function(err, page) {
        if (err)
            console.log(err);
        // console.log(page)
        res.render('index', {
            title: page.title,
            content: page.content
        })

    });

});




////////////////////////////////
module.exports = router;