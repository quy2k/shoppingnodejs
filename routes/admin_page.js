const express = require('express');
const router = express.Router()
const Page = require('../models/page')

/**
 * GET page index
 */
router.get('/', (req, res) => {
    Page.find({}).sort({ sorting: 1 }).exec((err, pages) => {
        // res.json(pages)
        res.render('admin/pages', { pages: pages });
    });
});


// router.get('/pages', (req, res) => {
//     Page.find({}).sort({ sorting: 1 }).exec((err, pages) => {
//         // res.json(pages)
//         res.render('admin/pages', { pages: pages });
//     });
// });
/**
 * GET add page
 */
router.get('/add-page', (req, res) => {
    const title = "";
    const slug = "";
    const content = "";
    res.render('admin/add_page', {
        title: title,
        slug: slug,
        content: content
    })
})

/**
 * POST add page
 */
router.post('/add-page', (req, res) => {
    console.log("vao day");
    req.checkBody('title', 'Title must hhave a value !').notEmpty();
    req.checkBody('content', 'Content must hhave a value !').notEmpty();

    console.log(req.body);
    var title = req.body.title;
    var slug = req.body.slug.replace(/\s+/g, '-').toLowerCase();
    if (slug == "") slug = title.replace(/\s+/g, '-').toLowerCase();
    var content = req.body.content;

    var errors = req.validationErrors();
    if (errors) {
        res.render('admin/add_page', {
            errors: errors,
            title: title,
            slug: slug,
            content: content
        })
    } else {
        // res.json({
        //     title: title,
        //     slug: slug,
        //     content: content
        // })
        Page.findById(req.params.id, (err, page) => {
            if (page) {
                res.flash('danger', 'Page slug exits, Plz choose slug another')
                res.render('admin/add_page', {
                    errors: errors,
                    title: title,
                    slug: slug,
                    content: content
                });
            } else {
                const page = new Page({
                    title: title,
                    slug: slug,
                    content: content,
                    sorting: 100
                });
                page.save(err => {
                    if (err)
                        return console.log(err);
                    req.flash('success', 'Page Added!');
                    res.redirect('/admin/pages')
                })
            }
        });
    }
});

/**
 * Post reooder pages
 * 
 */
router.post('/reorder-page', (req, res) => {
    var ids = req.body['id[]'];
    console.log(ids)
    var count = 0;


    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        count++;

        (function(count) {
            Page.findById(id, (err, page) => {
                page.sorting = count;
                page.save(err => {
                    if (err) return console.log(err)
                });
            });
        })(count);
    }
});

/**
 * 
 * GET edit page
 */
router.get('/edit-page/:id', async(req, res) => {
    console.log(req.params.id);
    await Page.findById(req.params.id, (err, page) => {
        if (err) { return console.log(err) }
        // console.log(page.title)

        res.render('admin/edit_page', { page: page })

    })

});

/**
 * 
 * POST edit page
 */
router.post('/edit-page/:id', async(req, res) => {
    console.log(req.body)
    await Page.findByIdAndUpdate(req.body.id, { title, sorting, slug, content } = req.body, (err, page) => {
        if (err) { return console.log(err) }
        console.log(page)
        res.redirect('/admin/pages');
    })
});
/**
 * 
 * POST delete page
 */
router.get('/delete-page/:id', async(req, res) => {
    console.log(req.params.id)
    await Page.findByIdAndDelete(req.params.id,
        (err, page) => {
            if (err) { return console.log(err) }

            res.redirect('/admin/pages');
        }
    )
});

////////////////////////////////
module.exports = router;