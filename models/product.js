const mongoose = require('mongoose')

//Product Schame

const ProductSchame = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    slug: {
        type: String,
    },
    desc: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    image: {
        type: String
    }
});

module.exports = mongoose.model('Product', ProductSchame)