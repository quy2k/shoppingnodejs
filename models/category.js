const mongoose = require('mongoose')

//Category Schame

const CategorySchame = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    slug: {
        type: String
    }
});

module.exports = mongoose.model('Category', CategorySchame)