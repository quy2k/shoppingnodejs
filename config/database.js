
const mongoose = require('mongoose')
const dotenv = require('dotenv').config()


const connectDB = async () => {

    const connectDB = await mongoose.connect(process.env.MONGOOSE_URI, {
        useCreateIndex: true,
        useFindAndModify: false,
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    try {
        if (connectDB) console.log(`Connected to database`);
    } catch (error) {
        console.log(error)
    }
}
module.exports = connectDB()