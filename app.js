/**************************************
            @Require Middleware
 **************************************/
const express = require('express')
const path = require('path')
const morgan = require('morgan')
const mongoose = require('mongoose')
const session = require('express-session')
const expressValidator = require('express-validator')
var flash = require('connect-flash');
const fileUpload = require('express-fileupload')

/**************************************
            @Connect Database
 **************************************/
const config = require('./config/database')

/**************************************
            @Require routes
 **************************************/
const pages = require('./routes/page')
const adminPages = require('./routes/admin_page')
const adminCategories = require('./routes/admin_category')
const adminProducts = require('./routes/admin_product')

//init app
const app = express()
    // getting-started.js

// Middleware Morgance

app.use(morgan('dev'))
    //View engine setup
app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'));

//Set public folders
app.use(express.static(path.join(__dirname, 'public')));

//Set global errors variable
app.locals.errors = null;



/**************************************
            @SET locals
 **************************************/

/************* Get Page Models *************/
var Page = require('./models/page');

//Get all pages to pass to header.js
Page.find({}).sort({ sorting: 1 }).exec((err, pages) => {
    if (err)
        console.log(err)
    app.locals.pages = pages
        // console.log(app.locals.pages)
});

/************* Get Categories Models *************/
var Category = require('./models/category');

// Get all categories to pass to header.js
Category.find(async(err, categories) => {
    if (err) {
        console.log(err)
    } else {
        app.locals.categories = categories
            // console.log(app.locals.categories)
    }
});



//Json files
app.use(express.json());
app.use(express.urlencoded({
        extended: true
    }))
    // session
app.set('trust proxy', 1) // trust first proxy
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: true
    }
}));

app.use(fileUpload())

// Express Validator Middleware

app.use(expressValidator({
    errorFormatter: (param, msg, value) => {
        const namespaces = param.split('.'),
            root = namespaces.shift(),
            formParam = root;
        while (namespaces.length) {
            formParam += '[' + namespaces.shift() + ']';
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        };
    },
    customValidators: {
        isImage: (value, filename) => {
            var extension = (path.extname(filename)).toLowerCase();
            switch (extension) {
                case '.jpg':
                    return '.jpg';
                case '.jpeg':
                    return '.jpeg';
                case '.png':
                    return '.png';
                case '':
                    return '.jpg';
                default:
                    return false;
            }
        }
    }
}));

//Set

app.use(flash());
app.use((req, res, next) => {
    res.locals.messages = require('express-messages')(req, res);
    next();
});


//Set routes
app.use('/', pages);
app.use('/admin/pages', adminPages);
app.use('/admin/categories', adminCategories);
app.use('/admin/products', adminProducts);

//Start server
const PORT = process.env.PORT || 3000
app.get('/', (req, res) => res.send('Hello World!'))
app.listen(PORT, () => console.log(`Example app listening on port ${PORT}`))